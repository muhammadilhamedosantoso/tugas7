<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>String PHP</title>
</head>
<body>
    <h2>Berlatih string PHP</h2>
    <?php

        echo "<h3>Soal No 1</h3>";
        $kalimat1 = "PHP is never old";
        echo "Kalimat pertama : " . $kalimat1 ."<br>";
        echo "Panjang string : " . strlen($kalimat1) . "," . "<br>";
        echo "Jumlah kata : " . str_word_count($kalimat1) . "<br><br>";

        $kalimat2 = "Hello PHP!";
        echo "Kalimat pertama : " . $kalimat2 . "<br>";
        echo "Panjang string : " . strlen($kalimat2) . "<br>";
        echo "Jumlah kata : " . str_word_count($kalimat2) . "<br>";
        echo "<br>";

        $kalimat3 = "I'm ready for the challenges";
        echo "Kalimat pertama : " . $kalimat3 . "<br>";
        echo "Panjang string : " . strlen($kalimat3) . "<br>";
        echo "Jumlah kata : " . str_word_count($kalimat3) . "<br>";

        echo "<h3>Contoh 2</h3>";
        $string2 = "I love PHP";
        echo "Kalimat pertama : " . $string2 . "<br>";
        echo "Kalimat kedua : " . substr($string2, 0, 1) . "<br>"; 
        echo "Kalimat ketiga : " . substr($string2,2,5) . "<br>";
        echo "kalimat keempat : " . substr($string2,7,9) . "<br>";

        echo "<h3>Contoh 3</h3>";
        $string3 = "PHP is old but sexy";
        echo "kalimat pertama : " . $string3 . "<br>";
        echo "kalimat kedua : " . str_replace("sexy", "awesome", $string3);
        
    ?>
</body>
</html>