<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array</title>
</head>
<body>
    <h1>Berlatih Array</h1>
    <?php

        echo "<h3>Soal 1</h3>";
        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
        $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
        print_r($kids);
        echo "<br>";
        print_r($adults);
        
        echo "<h3>Soal 2</h3>";
        echo "Cast Stranger Things: ";
        echo "<br>";
        echo "Total Kids : " . count($kids);
        echo "<ol>";
        echo "<li>" . $kids[0] . "<br>";
        echo "<li>" . $kids[1] . "<br>";
        echo "<li>" . $kids[2] . "<br>";
        echo "<li>" . $kids[3] . "<br>";
        echo "<li>" . $kids[4] . "<br>";
        echo "</ol>";

        echo "Total Adults : " . count($adults);
        echo "<ol>";
        echo "<li>" . $adults[0] . "<br>";
        echo "<li>" . $adults[1] . "<br>";
        echo "<li>" . $adults[2] . "<br>";
        echo "<li>" . $adults[3] . "<br>";
        echo "<li>" . $adults[4] . "<br>";
        echo "</ol>";

        echo "<h3>Soal 3</h3>";
        $biodata = [
            ["Name" => "Will Byers", "Age" => "12", "Aliases" => "Will the Wise", "Status" => "Alive"],
            ["Name" => "Miker wheeler", "Age" => "12", "Aliases" => "Dungeon Master", "Status" => "Alive"],
            ["Name" => "Jim Hopper", "Age" => "43", "Aliases" => "Chief Hopper", "Status" => "Deceased"],
            ["Name" => "Eleven", "Age" => "12", "Aliases" => "El", "Status" => "Alive"]
        ];

        echo "<pre>";
        print_r($biodata);
        echo "</pre>";
        
    ?>
</body>
</html>